DCAP
====

This package provides the DCAP library for the analysis release from
pre-compiled binaries provided by LCG.

So the package can only provide DCAP for SLC6 and CC7. On other platforms
the build just skips this package, and ROOT doesn't try to provide DCache
support.
