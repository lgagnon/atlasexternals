// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration.
// thread19_test: testing check_thread_safe_call,
//                  calling function of class marked as not thread-safe.

#pragma ATLAS check_thread_safety


class [[ATLAS::not_thread_safe]] NTS
{
public:
  void foo() const;
};

void nts_class_test (const NTS& p)
{
  p.foo();
}
