# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration file building the LAPACK library.
#

# Declare the name of the package:
atlas_subdir( Lapack )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Extend the CMAKE_Fortran_FLAGS variable with the build type specific
# flags, as we only use that variable in setting up the make.inc file.
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   string( TOUPPER ${CMAKE_BUILD_TYPE} _type )
   set( CMAKE_Fortran_FLAGS
      "${CMAKE_Fortran_FLAGS} ${CMAKE_Fortran_FLAGS_${_type}}" )
   unset( _type )
endif()

# Configure LAPACK's makefile:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/src/cmake-make.inc.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc @ONLY )

# Build LAPACK for the build area:
ExternalProject_Add( Lapack
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://cern.ch/atlas-software-dist-eos/externals/Lapack/lapack-lite-3.1.1.tgz
   URL_MD5 5feace3f4507a92ef822b2e0b50151d7
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E copy
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/make.inc make.inc
   BUILD_COMMAND make lapack_install lib
   INSTALL_COMMAND ${CMAKE_COMMAND} -E make_directory
   ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}
   COMMAND ${CMAKE_COMMAND} -E copy liblapack.a
   ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/
   COMMAND ${CMAKE_COMMAND} -E copy libtmglib.a
   ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/
   BUILD_IN_SOURCE 1
   )
add_dependencies( Lapack Blas )
add_dependencies( Package_Lapack Lapack )

# Install LAPACK:
install( FILES ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/liblapack.a
   ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/libtmglib.a
   DESTINATION ${CMAKE_INSTALL_LIBDIR} OPTIONAL )
