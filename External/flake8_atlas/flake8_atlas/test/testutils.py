# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

import unittest
import ast
import flake8.plugins.manager

class Flake8Test(unittest.TestCase):
   """Unit test case for flake8 checker"""

   _plugins = flake8.plugins.manager.Checkers()

   def _run(self, source, checker):
      """Run AST or line-based checker and return list of errors"""
      p = self._plugins.get(checker)
      kwargs = {}
      if 'tree' in p.parameter_names:
         kwargs['tree'] = ast.parse(source)
         if 'lines' in p.parameter_names:
            kwargs['lines'] = source.splitlines()
         chk = p.execute(**kwargs).run()
      else:
         chk = p.execute(source)
      return list(chk)

   def assertFail(self, source, checker):
      """Test that checker returns an error on source"""
      codes = self._run(source, checker)
      self.assertEqual(len(codes), 1)

   def assertPass(self, source, checker):
      """Test that checker passes on source"""
      codes = self._run(source, checker)
      self.assertListEqual(codes, [])
