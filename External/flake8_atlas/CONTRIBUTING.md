# ATLAS plugins for flake8
This module contains ATLAS-specific flake8_atlas plugins. It is built as part of the athena externals.

## Addition of new plugins
New plugins should be added following the existing examples:
1.  Add an `entry_point` to [setup.py](setup.py)
2.  Add the plugin code to one of the existing python files.
3.  Build the package
4.  Before releasing update the [version number](flake8_atlas/__init__.py)
5.  Update [README.md](README.md) with the new error code

To test a full local installation, use one the following:
*  from a git branch: `pip install git+[GITURL]@[BRANCH]#subdirectory=External/flake8_atlas`
*  from a local source: `pip install path/to/External/flake8_atlas`

## Other plugin documentation
*  http://flake8.pycqa.org/en/latest/plugin-development
*  AST-based plugins: https://greentreesnakes.readthedocs.io/en/latest/
