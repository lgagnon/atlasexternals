# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building GeoModel for ATLAS.
#

# Set a minimum required CMake version to use.
cmake_minimum_required( VERSION 3.12 )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()

# The name of the package:
atlas_subdir( GeoModel )

# External dependencies.
find_package( sqlite ) # we find the LCG package for SQlite3, called 'sqlite'
if( NOT ATLAS_BUILD_NLOHMANN_JSON )
   find_package( nlohmann_json )
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GeoModelBuild )

# Extra configuration parameters.
set( _extraOptions )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
    list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 14 )
   list( APPEND _extraOptions -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
    $ENV{CMAKE_PREFIX_PATH} ${SQLITE_LCGROOT} ${nlohmann_json_DIR} )
if( ( NOT ATLAS_BUILD_EIGEN ) AND EIGEN_LCGROOT )
   find_package( Eigen )
   list( APPEND _prefixPaths ${EIGEN_LCGROOT} )
endif()
if( ( NOT ATLAS_BUILD_XERCESC ) AND XERCESC_LCGROOT )
   find_package( XercesC )
   list( APPEND _prefixPaths ${XERCESC_LCGROOT} )
endif()

# Set up the build of GeoModel:
ExternalProject_Add( GeoModel
  PREFIX ${CMAKE_BINARY_DIR}
  INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
  GIT_REPOSITORY https://gitlab.cern.ch/GeoModelDev/GeoModel.git
  GIT_TAG 8a25c826
  CMAKE_CACHE_ARGS
  -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
  -DCMAKE_CXX_STANDARD_INCLUDE_DIRECTORIES:PATH=${SQLITE_INCLUDE_DIR}
  -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
  -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
  -DGEOMODEL_BUILD_TOOLS:BOOL=TRUE
  ${_extraOptions}
  LOG_CONFIGURE 1
  )
ExternalProject_Add_Step( GeoModel buildinstall
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
  COMMENT "Installing GeoModel into the build area"
  DEPENDEES install  )
ExternalProject_Add_Step( GeoModel purgebuild
  COMMAND ${CMAKE_COMMAND} -E echo "Removing previous build results for GeoModel"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
  DEPENDEES download
  DEPENDERS patch )

# add_dependencies
add_dependencies( GeoModel nlohmann_json )
if( ATLAS_BUILD_EIGEN )
   add_dependencies( GeoModel Eigen )
endif()
if( ATLAS_BUILD_XERCESC )
   add_dependencies( GeoModel XercesC )
endif()

# set "Package"
add_dependencies( Package_GeoModel GeoModel )

# Install GeoModel:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/FindGeoModel.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )
