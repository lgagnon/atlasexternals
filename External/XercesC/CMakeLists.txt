# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Set the name of the package.
atlas_subdir( XercesC )

# Stop right away if the package is not supposed to be built.
if( NOT ATLAS_BUILD_XERCESC )
   return()
endif()

# Tell the user what's happening.
message( STATUS "Building XercesC as part of this project" )

# Set the code's source.
set( _source
   "https://cern.ch/lcgpackages/tarFiles/sources/xerces-c-3.2.3.tar.gz" )
set( _md5 "a5fa4d920fce31c9ca3bfef241644494" )

# Directory for the temporary build results.
set( _buildDir ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/XercesCBuild )

# Set up the build of GeoModelTools:
ExternalProject_Add( XercesC
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   CONFIGURE_COMMAND
   ${CMAKE_COMMAND} -E env CXXFLAGS=-std=c++${CMAKE_CXX_STANDARD}
   <SOURCE_DIR>/configure --prefix=${_buildDir}
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( XercesC forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of XercesC (2020.09.07.)"
   DEPENDERS download )
ExternalProject_Add_Step( XercesC purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for XercesC"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_XercesC XercesC )

# Install XercesC:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
