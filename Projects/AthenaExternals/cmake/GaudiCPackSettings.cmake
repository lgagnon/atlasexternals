# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# This file is picked up by Gaudi, when it's built against
# AthenaExternals. It configures how Gaudi should use CPack.
#

# Tell the user what's happening:
message( STATUS "Configuring GAUDI to use the AthenaExternals CPack settings" )

# Remember the project name:
set( _projectName ${CMAKE_PROJECT_NAME} )
# Remember the project version:
set( _projectVersion ${CMAKE_PROJECT_VERSION} )
set( _projectVersionMajor ${CMAKE_PROJECT_VERSION_MAJOR} )
set( _projectVersionMinor ${CMAKE_PROJECT_VERSION_MINOR} )
set( _projectVersionPatch ${CMAKE_PROJECT_VERSION_PATCH} )

# Set the GAUDI version the same as the AthenaExternals version.
set( CMAKE_PROJECT_VERSION "${AthenaExternals_VERSION}" )

# Decode the version string into 3 numbers:
set( PROJECT_VERSION_REGEX "([0-9]+).([0-9]+).([0-9]+)" )
if( CMAKE_PROJECT_VERSION MATCHES ${PROJECT_VERSION_REGEX} )
   set( CMAKE_PROJECT_VERSION_MAJOR ${CMAKE_MATCH_1} )
   set( CMAKE_PROJECT_VERSION_MINOR ${CMAKE_MATCH_2} )
   set( CMAKE_PROJECT_VERSION_PATCH ${CMAKE_MATCH_3} )
endif()
unset( PROJECT_VERSION_REGEX )

# Call the package GAUDI, and not Gaudi:
set( CMAKE_PROJECT_NAME "GAUDI" )

# Make the GAUDI package depend on the AthenaExternals one:
set( ATLAS_BASE_PROJECTS AthenaExternals ${AthenaExternals_VERSION} )

# Do the regular ATLAS CPack configuration:
include( AtlasInternals )
atlas_cpack_setup()

# Set up a ReleaseData file for Gaudi:
atlas_generate_releasedata()

# Clean up:
set( CMAKE_PROJECT_VERSION ${_projectVersion} )
unset( _projectVersion )
set( CMAKE_PROJECT_VERSION_MAJOR ${_projectVersionMajor} )
unset( _projectVersionMajor )
set( CMAKE_PROJECT_VERSION_MINOR ${_projectVersionMinor} )
unset( _projectVersionMinor )
set( CMAKE_PROJECT_VERSION_PATCH ${_projectVersionPatch} )
unset( _projectVersionPatch )
set( CMAKE_PROJECT_NAME ${_projectName} )
unset( _projectName )
unset( ATLAS_BASE_PROJECTS )
